﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour
{

    //[SerializeField] BoxCollider collisionMesh;
    [SerializeField] int hitPoints;
    [SerializeField] ParticleSystem hitParticlePrefab;
    [SerializeField] ParticleSystem deathParticlePrefab;
    [SerializeField] AudioClip enemyHitSfx;
    [SerializeField] AudioClip enemyDeathSfx;

    AudioSource myAudioSource;
    static GameObject particleParent;

    void Start()
    {
        if (!particleParent)
        {
            particleParent = GameObject.Find("Death Effects");
        }
        myAudioSource = GetComponent<AudioSource>();
    }

    void OnParticleCollision(GameObject other)
    {
        print("I'm hit!");
        ProcessHit();
        if (hitPoints <= 0)
        {
            KillEnemy();
        }
    }

    void ProcessHit()
    {
        hitPoints = hitPoints - 1;
        hitParticlePrefab.Play();
        myAudioSource.PlayOneShot(enemyHitSfx);
    }

    void KillEnemy()
    {
        ParticleSystem vfx = Instantiate(deathParticlePrefab, transform.position, Quaternion.identity);
        vfx.transform.parent = particleParent.transform;
        vfx.Play();
        AudioSource.PlayClipAtPoint(enemyDeathSfx, Camera.main.transform.position, 0.75f);

        Destroy(gameObject);
    }
}
