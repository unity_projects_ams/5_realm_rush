﻿using UnityEngine;

[ExecuteInEditMode]
[SelectionBase]
[RequireComponent(typeof(Waypoint))]
public class CubeEditor : MonoBehaviour
{
    Waypoint waypoint;

    void Awake()
    {
        waypoint = GetComponent<Waypoint>();
    }

    // Update is called once per frame
    void Update()
    {
        SnapToGrid();
        UpdateLabel();

    }

    void SnapToGrid()
    {
        int gridSize = waypoint.GridSize;
        transform.position = new Vector3(
            waypoint.getGridPos().x * gridSize,
            0f,
            waypoint.getGridPos().y * gridSize
        );
    }

    void UpdateLabel()
    {
        TextMesh textMesh = GetComponentInChildren<TextMesh>();
        string labelText = string.Concat((waypoint.getGridPos().x).ToString(), ",", (waypoint.getGridPos().y).ToString());
        textMesh.text = labelText;
        gameObject.name = labelText;
    }
}
