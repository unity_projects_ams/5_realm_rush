﻿using UnityEngine;
using UnityEngine.Experimental.UIElements;

public class Waypoint : MonoBehaviour
{
    public Waypoint exploredFrom;
    public bool isExplored = false;
    public bool isPlaceable = true;

    const int gridSize = 10;

    Vector2Int gridPos;

    public int GridSize
    {
        get
        {
            return gridSize;
        }
    }

    public Vector2Int getGridPos()
    {
        return new Vector2Int(
            Mathf.RoundToInt(transform.position.x / gridSize),
            Mathf.RoundToInt(transform.position.z / gridSize)
        );
    }

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown((int)MouseButton.LeftMouse) && isPlaceable)
        {
            FindObjectOfType<TowerFactory>().AddTower(this);
        }
    }

}
