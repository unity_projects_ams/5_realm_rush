﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerFactory : MonoBehaviour
{

    [SerializeField] Tower towerPrefab;
    [SerializeField] int towerLimit;

    Queue<Tower> towerQueue = new Queue<Tower>();

    [SerializeField] Transform towerParent;

    public void AddTower(Waypoint baseWaypoint)
    {

        int queueSize = towerQueue.Count;

        if (towerLimit > queueSize)
        {
            InstantiateNewTower(baseWaypoint);
        }
        else
        {
            MoveExistingTower(baseWaypoint);
        }
    }

    void InstantiateNewTower(Waypoint baseWaypoint)
    {
        Tower createdTower = Instantiate(towerPrefab, baseWaypoint.transform.position, Quaternion.identity);
        createdTower.transform.parent = towerParent;

        createdTower.baseWaypoint = baseWaypoint;
        baseWaypoint.isPlaceable = false;

        towerQueue.Enqueue(createdTower);
    }

    void MoveExistingTower(Waypoint newBaseWaypoint)
    {
        Tower bottomTower = towerQueue.Dequeue();
        bottomTower.baseWaypoint.isPlaceable = true;

        newBaseWaypoint.isPlaceable = false;
        bottomTower.transform.position = newBaseWaypoint.transform.position;
        bottomTower.baseWaypoint = newBaseWaypoint;

        towerQueue.Enqueue(bottomTower);
    }
}
