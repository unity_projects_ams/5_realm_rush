﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawner : MonoBehaviour
{

    [Range(0.1f, 15f)] [SerializeField] float secondsBetweenSpawns;
    [SerializeField] EnemyMovement enemyPrefab;
    [SerializeField] int amountOfEnemies;
    [SerializeField] Text spawnText;
    [SerializeField] AudioClip spawnEnemySfx;
    int spawnedEnemies = 0;



    // Use this for initialization
    void Start()
    {
        spawnText.text = spawnedEnemies.ToString();
        StartCoroutine(RepeatedlySpawnEnemies());
    }

    IEnumerator RepeatedlySpawnEnemies()
    {

        while (amountOfEnemies > spawnedEnemies)
        {
            AddScore();
            GetComponent<AudioSource>().PlayOneShot(spawnEnemySfx);
            EnemyMovement spawnedEnemy = Instantiate(enemyPrefab, transform.position, Quaternion.identity);
            spawnedEnemy.transform.localEulerAngles = new Vector3(0f, 90f, 0f);
            spawnedEnemy.transform.parent = transform;

            yield return new WaitForSeconds(secondsBetweenSpawns);
        }

    }

    private void AddScore()
    {
        ++spawnedEnemies;
        spawnText.text = spawnedEnemies.ToString();
    }
}
