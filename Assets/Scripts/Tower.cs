﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Tower : MonoBehaviour
{
    // Parameters
    [SerializeField] Transform objectToPan;
    [SerializeField] float attackRange;
    [SerializeField] ParticleSystem projectileParticle;
    float distanceBetween;

    public Waypoint baseWaypoint;

    // State
    Transform targetEnemy;

    // Update is called once per frame
    void Update()
    {
        SetTargetEnemy();

        if (targetEnemy)
        {
            objectToPan.LookAt(targetEnemy);
            FireAtEnemy();
        }
        else
        {
            Shoot(false);
        }
    }

    void SetTargetEnemy()
    {
        EnemyDamage[] sceneEnemies = FindObjectsOfType<EnemyDamage>();
        if (!sceneEnemies.Any()) return;

        Transform closestEnemy = sceneEnemies[0].transform;

        foreach (EnemyDamage nextEnemy in sceneEnemies)
        {

            closestEnemy = GetClosestEnemy(closestEnemy, nextEnemy.transform);

        }

        targetEnemy = closestEnemy;
    }

    Transform GetClosestEnemy(Transform transformA, Transform transformB)
    {
        float distToA = Vector3.Distance(transformA.position, transform.position);
        float distToB = Vector3.Distance(transformB.position, transform.position);
        if (distToB < distToA)
        {
            transformA = transformB;
        }
        return transformA;
    }

    void FireAtEnemy()
    {
        float distanceToEnemy = Vector3.Distance(targetEnemy.position, transform.position);
        if (distanceToEnemy <= attackRange)
        {
            Shoot(true);
        }
        else
        {
            Shoot(false);
        }
    }

    void Shoot(bool isActive)
    {
        ParticleSystem.EmissionModule emissionModule = projectileParticle.emission;
        emissionModule.enabled = isActive;
    }
}
