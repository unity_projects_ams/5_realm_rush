﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

    [SerializeField] int health = 10;
    [SerializeField] int healtDecrease = 1;
    [SerializeField] Text healthText;
    [SerializeField] AudioClip playerDamageSfx;

    private void Start()
    {
        healthText.text = health.ToString();
    }

    void OnTriggerEnter(Collider other)
    {
        GetComponent<AudioSource>().PlayOneShot(playerDamageSfx);
        health -= healtDecrease;
        healthText.text = health.ToString();
    }

}
